import React, { useState } from 'react';

import PropTypes from 'prop-types';

// ****************************************************************** //

import Error from './Error';

// ****************************************************************** //

import { nanoid } from 'nanoid'

const Formulario = ( {guardarGasto, guardarCrearGasto} ) => {

    const [ nombre, guardarNombre ] = useState( '' );
    const [ cantidad, guardarCantidad ] = useState(0);
    const [ error, guardarError ] = useState(false);

    // WHEN USER ADDS AN UPKEEP
    const agregarGastos = e => {
        e.preventDefault();

        // * VALIDATE
        if( cantidad < 1 || isNaN(cantidad) || nombre.trim() === '' ){

            guardarError(true);
            return;

        }

        guardarError(false);
        // * SEND THE UPKEEP TO THE MAIN COMPONENT
        const gasto = {
            nombre,
            cantidad,
            id: nanoid()
        }

        guardarGasto(gasto);
        guardarCrearGasto(true);

        // * RESET FORM
        guardarNombre('');
        guardarCantidad(0);

    }

    return(
        <form
            onSubmit={agregarGastos}
        >
            <h2> Agrega tus gastos aquí </h2>

            { error ? <Error mensaje="Ambos campos son obligatorios o Presupuesto incorrecto" /> : null }

            <div className="campo">
                <label> Nombre Gasto </label>
                <input type="text"
                       className="u-full-width"
                       placeholder="Ej. Transporte"
                       value={nombre}
                       onChange={ e => guardarNombre( e.target.value ) }
                >
                </input>
            </div>

            <div className="campo">
                <label> Cantidad Gasto </label>
                <input type="number"
                       className="u-full-width"
                       min="0"
                       value={cantidad}
                       onChange={ e => guardarCantidad( parseInt(e.target.value, 10) ) }
                >
                </input>
            </div>

            <input 
                type="submit"
                className="button-primary u-full-width"
                value="Agregar Gasto"
            />

        </form>
    );

}

Formulario.propTypes = {

    guardarGasto: PropTypes.func.isRequired,
    guardarCrearGasto: PropTypes.func.isRequired

}

export default Formulario;